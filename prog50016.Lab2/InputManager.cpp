#include "InputManager.h"

InputManager::InputManager()
{
	std::cout << "InputManager created" << std::endl;
}

InputManager::~InputManager()
{
	std::cout << "InputManager deleted" << std::endl;
}

void InputManager::initialize()
{
	std::cout << "InputManager initialized" << std::endl;
}

void InputManager::display()
{
	std::cout << "InpuitManager was displayed" << std::endl;
}

void InputManager::update()
{
	std::cout << "InputManager was updated" << std::endl;
}

void InputManager::load(json::JSON& _json)
{
	std::cout << "InputManager was loaded" << std::endl;
}