#pragma once
#ifndef _FILESYSTEM_H_
#define	_FILESYSTEM_H_

#include "StandardIncludes.h"

class FileSystem
{
public:
	// Constructors / Destructors
	FileSystem();
	virtual ~FileSystem();

	// Methods
	void initialize();
	void display();
	void update();
	void load(json::JSON& _json);
};

#endif // !_FILESYSTEM_H_