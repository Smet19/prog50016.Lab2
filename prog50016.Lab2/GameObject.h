#pragma once
#ifndef _GAMEOBJECT_H_
#define	_GAMEOBJECT_H_

#include <string>
#include <list>
#include "StandardIncludes.h"
#include "Component.h"

class GameObject
{
public:
	// Constructors / Destructors
	GameObject();
	virtual ~GameObject();

	// Methods
	void update();
	void display();
	void load(json::JSON& _json);
	std::string getName() { return name; }
	void addComponent(Component* _component);
	void removeComponent(Component* _component);

private:
	std::string name;
	std::list<Component*> components;
};

#endif // !_GAMEOBJECT_H_