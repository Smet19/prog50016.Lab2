#include "Component.h"

Component::Component()
{
	id = 0;
	std::cout << "Component created" << std::endl;
}

Component::~Component()
{
	std::cout << "Component " + std::to_string(id) + " deleted" << std::endl;
}

void Component::initialize()
{
	std::cout << "Component initialized" << std::endl;
}

void Component::display()
{
	std::cout << "Component " + std::to_string(id) + " was displayed" << std::endl;
}

void Component::update()
{
	std::cout << "Component " + std::to_string(id) + " was updated" << std::endl;
}

void Component::load(json::JSON& _json)
{
	if (_json.hasKey("id"))
	{
		id = _json["id"].ToInt();
	}
	std::cout << "Component " + std::to_string(id) + " loaded" << std::endl;
}