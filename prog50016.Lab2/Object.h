#pragma once
#ifndef _OBJECT_H_
#define _OBJECT_H_

#include "StandardIncludes.h"

class Object
{
public:
	// Constructors / Destructors
	Object();
	virtual ~Object();

	// Methods
	bool isInitialized() { return initialized; }
	void initialize();
	void display();
	void load(json::JSON& _json);

private:
	bool initialized;
};
#endif // !_OBJECT_H_

