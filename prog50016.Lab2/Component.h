#pragma once
#ifndef _COMPONENT_H_
#define	_COMPONENT_H_

#include "StandardIncludes.h"

class Component
{
public:
	// Constructors / Destructors
	Component();
	virtual ~Component();

	// Methods
	void initialize();
	int getComponentId() { return id; }
	void display();
	void update();
	void load(json::JSON& _json);

private:
	int id;
};

#endif // !_COMPONENT_H_

