#pragma once
#ifndef _GAMEENGINE_H_
#define _GAMEENGINE_H_

#include "StandardIncludes.h"
#include "RenderSystem.h"
#include "FileSystem.h"
#include "InputManager.h"
#include "AssetManager.h"
#include "GameObjectManager.h"

class GameEngine
{
public:
	// Constructors / Destructors
	GameEngine();
	virtual ~GameEngine();

	// Methods
	void initialize();
	void display();
	void load(json::JSON& _json);
	void gameLoop();

private:
	RenderSystem* renderSystem;
	FileSystem* fileSystem;
	InputManager* inputManager;
	AssetManager* assetManager;
	GameObjectManager* gameObjectManager;
};
#endif // !_GAMEENGINE_H_

