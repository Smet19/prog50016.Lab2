#pragma once
#ifndef _RENDERSYSTEM_H_
#define	_RENDERSYSTEM_H_

#include <string>
#include "StandardIncludes.h"

class RenderSystem
{
public:
	// Constructors / Destructors
	RenderSystem();
	virtual ~RenderSystem();

	// Methods
	void initialize();
	void display();
	void update();
	void load(json::JSON& _json);

private:
	std::string name;
	int width;
	int height;
	bool fullscreen;
};
#endif // !_RENDERSYSTEM_H_

