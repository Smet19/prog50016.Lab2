#include "FileSystem.h"

FileSystem::FileSystem()
{
	std::cout << "FileSystem created" << std::endl;
}

FileSystem::~FileSystem()
{
	std::cout << "FileSystem deleted" << std::endl;
}

void FileSystem::initialize()
{
	std::cout << "FileSystem initialized" << std::endl;
}

void FileSystem::display()
{
	std::cout << "FileSystem was displayed" << std::endl;
}

void FileSystem::update()
{
	std::cout << "FileSystem was updated" << std::endl;
}

void FileSystem::load(json::JSON& _json)
{
	std::cout << "FileSystem loaded" << std::endl;
}