#include "AssetManager.h"

AssetManager::AssetManager()
{
	std::cout << "AssetManager created" << std::endl;
}

AssetManager::~AssetManager()
{
	std::cout << "AssetManager deleted" << std::endl;
}

void AssetManager::initialize()
{
	std::cout << "AssetManager initialized" << std::endl;
}

void AssetManager::display()
{
	std::cout << "AssetManager was displayed" << std::endl;
}

void AssetManager::update()
{
	std::cout << "AssetManager was updated" << std::endl;
}

void AssetManager::load(json::JSON& _json)
{
	std::cout << "AssetManager loaded" << std::endl;
}