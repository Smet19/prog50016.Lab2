#include "RenderSystem.h"

RenderSystem::RenderSystem()
{
	name = "DefaultName";
	width = 640;
	height = 480;
	fullscreen = false;

	std::cout << "RenderSystem created" << std::endl;
}

RenderSystem::~RenderSystem()
{
	std::cout << "RenderSystem deleted" << std::endl;
}

void RenderSystem::initialize()
{
	std::cout << "RenderSystem initialized" << std::endl;
}

void RenderSystem::display()
{
	std::cout << "RenderSystem displayed" << std::endl;
}

void RenderSystem::update()
{
	std::cout << "RenderSystem updated" << std::endl;
}

void RenderSystem::load(json::JSON& _json)
{
	if (_json.hasKey("RenderSystem"))
	{
		json::JSON renderSystemJSON = _json["RenderSystem"];
		if (renderSystemJSON.hasKey("Name"))
		{
			name = renderSystemJSON["Name"].ToString();
		}

		if (renderSystemJSON.hasKey("width"))
		{
			width = renderSystemJSON["width"].ToInt();
		}

		if (renderSystemJSON.hasKey("height"))
		{
			height = renderSystemJSON["height"].ToInt();
		}

		if (renderSystemJSON.hasKey("fullscreen"))
		{
			fullscreen = renderSystemJSON["fullscreen"].ToBool();
		}
	}

	std::cout << "RenderSystem loaded" << std::endl;
}