#include "StandardIncludes.h"
#include "GameEngine.h"

// Your main function should create an instance of an object of GameEngine and call initialize.
// After you have loaded the GameEngine and all other objects/classes call the GameLoop method from
// your main functionand, run the loop for 5 iterations.
// After you exit back to your main, call display on GameEngine
int main()
{
	GameEngine engine;

	engine.initialize();
	
	for (int i = 0; i < 5; i++)
	{
		engine.gameLoop();
	}
	
	engine.display();
}
