#include "GameObject.h"

GameObject::GameObject()
{
	std::cout << "GameObject created" << std::endl;
}

GameObject::~GameObject()
{
	for (auto* component : components)
	{
		// removeComponent(component);
		delete component;
	}

	std::cout << "GameObject " + name + " deleted" << std::endl;
}

void GameObject::display()
{
	std::cout << "GameObject" + name + " was displayed" << std::endl;
}

void GameObject::update()
{
	for (auto* component : components)
	{
		component->update();
	}

	std::cout << "GameObject " + name + " was updated" << std::endl;
}

void GameObject::load(json::JSON& _json)
{
	if (_json.hasKey("Name"))
	{
		name = _json["Name"].ToString();
	}

	if (_json.hasKey("Components"))
	{
		for (auto& classObject : _json["Components"].ArrayRange())
		{
			if (classObject.hasKey("className"))
			{
				std::string className = classObject["className"].ToString();
				if (className == "Component")
				{
					Component* comp = new Component();
					comp->load(classObject);
					addComponent(comp);
				}
			}
		}
	}
	std::cout << "GameObject " + name + " loaded" << std::endl;
}

void GameObject::addComponent(Component* _component)
{
	components.push_back(_component);
	std::cout << "Component " + std::to_string(_component->getComponentId()) + " was added to GameObject's " + name + " list of components" << std::endl;
}

void GameObject::removeComponent(Component* _component)
{
	components.remove(_component);
	std::cout << "Component " + std::to_string(_component->getComponentId()) + " was removed from GameObject's " + name + " list of components" << std::endl;
}