#pragma once
#ifndef _INPUTMANAGER_H_
#define	_INPUTMANAGER_H_

#include "StandardIncludes.h"

class InputManager
{
public:
	// Constructors / Destructors
	InputManager();
	virtual ~InputManager();

	// Methods
	void initialize();
	void display();
	void update();
	void load(json::JSON& _json);
};

#endif // !_INPUTMANAGER_H_

