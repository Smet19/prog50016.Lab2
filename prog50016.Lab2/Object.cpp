#include "Object.h"

Object::Object() 
{
	initialized = false;
	std::cout << "Object created" << std::endl;
}

Object::~Object()
{
	std::cout << "Object deleted" << std::endl;
}

void Object::initialize()
{
	//TODO �������������
	initialized = true;
	std::cout << "Object initialized" << std::endl;
}

void Object::display()
{
	std::cout << "Object displayed" << std::endl;
}

void Object::load(json::JSON& _json)
{
	std::cout << "Object loaded" << std::endl;
}
