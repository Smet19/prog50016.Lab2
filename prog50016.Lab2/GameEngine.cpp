#include "GameEngine.h"

GameEngine::GameEngine()
{
	renderSystem = new RenderSystem();
	fileSystem = new FileSystem();
	inputManager = new InputManager();
	assetManager = new AssetManager();
	gameObjectManager = new GameObjectManager();

	std::cout << "GameEngine created" << std::endl;
}

GameEngine::~GameEngine()
{
	delete renderSystem;
	delete fileSystem;
	delete inputManager;
	delete assetManager;
	delete gameObjectManager;

	std::cout << "GameEngine deleted" << std::endl;
}

// GameEngine::initialize must load the GameSettings.json
// IMPORTANT: When loading data, the GameEngine::initialize should not parse all the information,
// data should be loaded in the appropriate CLASS::load method.
// The settings file has the next file to load.We are not hardcoding the starting game level into code.
// Once you have determined what the next file to load, the GameEngine::initialize method must load
// the level file.
void GameEngine::initialize()
{
	// Initialize main systems
	renderSystem->initialize();
	fileSystem->initialize();
	inputManager->initialize();
	assetManager->initialize();
	gameObjectManager->initialize();

	// Load JSON file
	std::ifstream inputStream("./Assets/GameSettings.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON JSON = json::JSON::Load(str);

	// Load each individual system
	load(JSON);
	fileSystem->load(JSON);
	assetManager->load(JSON);
	gameObjectManager->load(JSON);
	renderSystem->load(JSON);
	inputManager->load(JSON);

	std::cout << "GameEngine initialized" << std::endl;
}

// Display should do what update does and call the proper classes 
// and in a proper order to see the objects.
void GameEngine::display()
{
	fileSystem->display();
	assetManager->display();
	gameObjectManager->display();
	renderSystem->display();
	inputManager->display();

	std::cout << "GameEngine called display methods on it's components" << std::endl;
}


// Your game loop should call update on the each of the systems and each system should call update on
// any objects it can call update on.Also, each game object should call update on each component.
void GameEngine::gameLoop()
{
	renderSystem->update();
	fileSystem->update();
	inputManager->update();
	assetManager->update();
	gameObjectManager->update();

	std::cout << "GameEngine called update methods on it's components" << std::endl;
}

// data should be loaded in the appropriate CLASS::load method
// The settings file has the next file to load. We are not hardcoding the starting game level into code.
// Your load method should output a message stating that the class was created/loaded
// (regardless if it has data to load).
void GameEngine::load(json::JSON& _json)
{
	if (_json.hasKey("GameEngine"))
	{
		json::JSON gameEngineJSON = _json["GameEngine"];

		if (gameEngineJSON.hasKey("DefaultFile"))
		{
			std::string fileName = gameEngineJSON["DefaultFile"].ToString();
			std::ifstream inputStream("./Assets/" + fileName);
			std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			json::JSON file = json::JSON::Load(str);

			if (file.hasKey("GameObjects"))
			{
				json::JSON managerSettings = file["GameObjects"];
				gameObjectManager->load(managerSettings);
			}
		}
	}

	std::cout << "GameEngine loaded" << std::endl;
}