#pragma once
#ifndef _GAMEOBJECTMANAGER_H_
#define	_GAMEOBJECTMANAGER_H_

#include <list>
#include "StandardIncludes.h"
#include "GameObject.h"

class GameObjectManager
{
public:
	// Constructors / Destructors
	GameObjectManager();
	virtual ~GameObjectManager();

	// Methods
	void initialize();
	void update();
	void display();
	void load(json::JSON& _json);
	void addGameObject(GameObject* _component);
	void removeGameObject(GameObject* _component);

private:
	std::list<GameObject*> gameObjects;
};

#endif // !_GAMEOBJECTMANAGER_H_

