#pragma once
#ifndef _ASSETMANAGER_H_
#define	_ASSETMANAGER_H_

#include "StandardIncludes.h"

class AssetManager
{
public:
	// Constructors / Destructors
	AssetManager();
	virtual ~AssetManager();

	// Methods
	void initialize();
	void display();
	void update();
	void load(json::JSON& _json);
};

#endif // !_ASSETMANAGER_H_

