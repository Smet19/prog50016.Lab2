#include "GameObjectManager.h"

GameObjectManager::GameObjectManager()
{
	std::cout << "GameObjectManager created" << std::endl;
}

GameObjectManager::~GameObjectManager()
{
	for (auto* gameObject : gameObjects)
	{
		// removeGameObject(gameObject);
		delete gameObject;
	}
	gameObjects.clear();

	std::cout << "GameObjectManager deleted" << std::endl;
}

void GameObjectManager::initialize()
{
	std::cout << "GameObjectManager initialized" << std::endl;
}

void GameObjectManager::display()
{
	std::cout << "GameObjectManager was displayed" << std::endl;
}

void GameObjectManager::update()
{
	for (auto* gameObject : gameObjects)
	{
		gameObject->update();
	}
	std::cout << "GameObjectManager was updated" << std::endl;
}

void GameObjectManager::load(json::JSON& _json)
{
	for (auto& classObject : _json.ArrayRange())
	{
		if (classObject.hasKey("className"))
		{
			std::string className = classObject["className"].ToString();
			if (className == "GameObject")
			{
				GameObject* gameObject = new GameObject();
				gameObject->load(classObject);
				addGameObject(gameObject);
			}
		}
	}
	std::cout << "GameObjectManager loaded" << std::endl;
}

void GameObjectManager::addGameObject(GameObject* _component)
{
	gameObjects.push_back(_component);
	std::cout << "GameObject " + _component->getName() + " was added to GameObjectManager's list of objects" << std::endl;
}

void GameObjectManager::removeGameObject(GameObject* _component)
{
	gameObjects.remove(_component);
	std::cout << "GameObject " + _component->getName() + " was removed from GameObjectManager's list of objects" << std::endl;
}